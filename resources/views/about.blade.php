@extends('layouts.main')

@section('content')
    

<!-- Page Title Starts -->
<section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
    <h1>Profil <span>Singkat </span></h1>
    <span class="title-bg">Profil Singkat</span>
</section>
<!-- Page Title Ends -->
<!-- Main Content Starts -->
<section class="main-content revealator-slideup revealator-once revealator-delay1">
    <div class="container">
    <div class="row">
            <!-- Personal Info Starts -->
            <div class="col-15 col-lg-5 col-xl-6">
                <div class="row">
                    <div class="col-12">
                        <h3 class="text-uppercase custom-title mb-0 ft-wt-600">My Profil</h3>
                    </div>
                    <div class="col-12 d-block d-sm-none">
                        <img src="img/muka.jpeg" class="img-fluid main-muka" alt="my picture" />
                    </div>
                    <div class="col-8">
                        <ul class="about-list list-unstyled open-sans-font">
                            <li> <span class="title">Fullname :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Putu Deva Tarinda Novani</span> </li>
                            <li> <span class="title">Nickname :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Deva</span> </li>
                            <li> <span class="title">Tempat Lahir :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Penatahan</span> </li>
                            <li> <span class="title">Tanggal Lahir :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">02 November 2000</span> </li>
                            <li> <span class="title">Alamat :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Sanggulan, Kediri, Tabanan</span> </li>
                            <li> <span class="title">Email :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">devatarinda51@gmail.com</span> </li>         
                        </ul>
                    </div>
                    </div>
            </div>
            <!-- Personal Info Ends -->
            <!-- Boxes Starts -->
            <div class="col-12 col-lg-7 col-xl-6 mt-5 mt-lg-0">
                <div class="row">
                    <div class="col-6,1">
                        <div class="box-stats with-margin">
                            <h3 class="poppins-font position-relative">SD</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">SDN 1 Dajan Peken</p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="box-stats with-margin">
                            <h3 class="poppins-font position-relative">SMP</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">SMPN 1 Tabanan</p>
                        </div>
                    </div>
                    <div class="col-6,3">
                        <div class="box-stats">
                            <h3 class="poppins-font position-relative">SMA</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">SMAN 1 Tabanan</p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="box-stats">
                            <h3 class="poppins-font position-relative">NOW</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">Undiksha</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Boxes Ends -->
            <div class="col-12 mt-3">
                        <a href="about" class="btn btn-download ">Download CV</a>
             </div>
        </div>
        <hr class="separator">
        <!-- Skills Starts -->
        <div class="row">
            <div class="col-12">
                <h3 class="text-uppercase pb-5 mb-0 text-left text-sm-center custom-title ft-wt-600">Beberapa fakta tentang seni</h3>
            </div>
            <div class="col-lg-6 m-15px-tb">
                <div class="resume-box">
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">Keahlian</span>
                            <p class="open-sans-font"> Seni merupakan suatu keahlian seseorang dalam membuat karya yang bermutu akan dilihat dari segi kehalusannya, keindahannya, fungsinya, bentuknya, maknanya, dan lain sebagainya.</p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">Daya Cipta</span>
                            <p class="open-sans-font">Seni dapat diartikan sebagai suatu hasil ciptaan manusia yang mengandung unsur keindahan dan juga dapat mempengaruhi perasaan orang lain.</p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">Beragam</span>
                            <p class="open-sans-font">Seni tidak bisa dikategorikan hanya dengan 1 bidang, melainkan banyak bidang dalam kehidupan.</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 m-15px-tb">
                <div class="resume-box">
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="fa fa-asterisk" aria-hidden="true"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">Macam-macam Seni</span>
                            <p class="open-sans-font">
                                <ul>
                                    <li>Seni Gerak</li>
                                    <li>Seni Rupa</li>
                                    <li>Seni Music</li>
                                    <li>Seni Sastra</li>
                                  </ul>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Experience & Education Ends -->
    </div>
</section>
<!-- Main Content Ends -->

@endsection